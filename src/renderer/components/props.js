export const mixinProps = {
    id: {
        required: false,
        default: () => { return (new Date).getTime() }
    },
    name: {
        type: String,
        required: false,
        default: ''
    },
    size: {
        type: Number,
        required: false,
        default: 16
    }
};